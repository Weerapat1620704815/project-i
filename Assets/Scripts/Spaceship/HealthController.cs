using UnityEngine;
using UnityEngine.UI;

namespace Spaceship
{
    public class HealthController : MonoBehaviour
    {
        [SerializeField] private EnemySpaceship enemyShip;
        [SerializeField] private Slider healthBar;

        internal void SetMaxHealth()
        {
            healthBar.maxValue = enemyShip.Hp;
            healthBar.value = enemyShip.Hp;
        }
    
        internal void SetHealth()
        {
            healthBar.value = enemyShip.Hp;
        }
    }
}
