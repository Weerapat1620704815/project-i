using System;
using Enemy;
using UnityEngine;
using UnityEngine.Serialization;

namespace Spaceship
{
    public class EnemySpaceship : Basespaceship, IDamagable
    {
        public event Action OnExploded;

        [SerializeField] private double fireRate = 1;
        private float fireCounter = 0;
        [SerializeField] private AudioClip enemyFireSound;
        [SerializeField] private float enemyFireSoundVolume = 0.3f;
       
        [SerializeField] private AudioClip enemyExplodeSound;
        [SerializeField] private float enemyExplodeSoundVolume = 0.3f;

        [SerializeField] private HealthController healtBar;

        public void Init(int hp, float speed,Transform target)
        {
            base.Init(hp, speed, defaultBullet);
            GetComponent<EnemyController>().player = target;
            healtBar.SetMaxHealth();
        }
        public void TakeHit(int damage)
        {
            Hp -= damage;
            healtBar.SetHealth();

            if (Hp > 0)
            {
                return;
            }
            
            Explode();
        }

        public void Explode()
        {
            AudioSource.PlayClipAtPoint(enemyExplodeSound, transform.position, enemyFireSoundVolume);
            Debug.Assert(Hp <= 0, "HP is more than zero");
            gameObject.SetActive(false);
            Destroy(gameObject);
            OnExploded?.Invoke();
        }

        public override void Fire()
        {
            // TODO: Implement this later
            fireCounter += Time.deltaTime;
            if (fireCounter >= fireRate)
            {
                AudioSource.PlayClipAtPoint(enemyFireSound, transform.position, enemyExplodeSoundVolume);
                var bullet = Instantiate(defaultBullet, gunPosition.position, transform.rotation);
                bullet.Init(Vector2.down);
                fireCounter = 0;
            }
        }
    }
}