using System;
using System.Collections;
using Unity.Mathematics;
using UnityEngine;

namespace Spaceship
{
    public class PlayerSpaceship : Basespaceship, IDamagable
    {
        public event Action OnExploded;

        [SerializeField] private AudioClip playerFireSound;
        [SerializeField] private float playerFireSoundVolume = 0.3f;
        [SerializeField] private AudioClip playerExplodeSound;
        [SerializeField] private float playerExplodeSoundVolume = 0.3f;

        [SerializeField] private Missile myMissile;
        internal bool HasMissile = true;
        private void Awake()
        {
            Debug.Assert(defaultBullet != null, "defaultBullet cannot be null");
            Debug.Assert(gunPosition != null, "gunPosition cannot be null");
        }

        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }

        public void ShootMissile()
        {
            StartCoroutine(ShootingMissile());
        }

        private IEnumerator ShootingMissile()
        {
            if (HasMissile)
            {
                HasMissile = false;
                var missile = Instantiate(myMissile,gunPosition.position,quaternion.identity);
                missile.Init();
                yield return new WaitForSeconds(4);
                HasMissile = true;
            }
        }

        public override void Fire()
        {
            AudioSource.PlayClipAtPoint(playerFireSound,gunPosition.position,playerFireSoundVolume);
            var bullet = Instantiate(defaultBullet,transform.position,Quaternion.identity);
            bullet.Init(Vector2.up);
        }

        public void TakeHit(int damage)
        {
            Hp -= damage;
            if (Hp > 0)
            {
                return;
            }
            Explode();
        }

        public void Explode()
        {
            Debug.Assert(Hp <= 0, "HP is more than zero");
            Destroy(gameObject);
            OnExploded?.Invoke();
            AudioSource.PlayClipAtPoint(playerExplodeSound,transform.position,playerExplodeSoundVolume);
        }
    }
}