using System.Collections;
using UnityEngine;

namespace Spaceship
{
    public class Missile : MonoBehaviour
    {
        [SerializeField] private int damage;
        [SerializeField] private float speed;
        [SerializeField] private Rigidbody2D rb;
        [SerializeField] private GameObject target;
        [SerializeField] private ParticleSystem boom;
        [SerializeField] private ParticleSystem trust;
        
        [SerializeField] private AudioClip explosion;
        [SerializeField] [Range(0.0f, 1.0f)] private float volume;

        private Vector2 direction;

        private bool setRotation = false;

        public void Init()
        {
            SetDirection();
            StartCoroutine(StartEngine());
        }

        IEnumerator StartEngine()
        {
            rb.velocity = transform.up * 0.8f;
            yield return new WaitForSeconds(1.2f);
            setRotation = true;
            trust.Play();
            yield return new WaitForSeconds(0.8f);
            setRotation = false;
            yield return new WaitForSeconds(0.1f);
            Move();
        }

        private void Awake()
        {
            trust = this.GetComponentInChildren<ParticleSystem>();
            trust.Stop();
            if (GameObject.FindWithTag("Enemy"))
            {
                target = GameObject.FindWithTag("Enemy");
            }
            rb = GetComponent<Rigidbody2D>();
            Debug.Assert(rb != null, "rigidbody2D cannot be null");
            Debug.Assert(trust != null, "trust != null");
            Debug.Assert(target != null, "target cannot be null");
        }

        private void Update()
        {
            SetDirection();
            Quaternion lookRotation;
            if (setRotation)
            {
                lookRotation = Quaternion.LookRotation(Vector3.forward, -direction);
                transform.rotation = Quaternion.Slerp(transform.rotation,lookRotation, Time.fixedDeltaTime * 0.5f);
            }
            lookRotation = Quaternion.LookRotation(Vector3.forward, -direction);
            transform.rotation = Quaternion.Slerp(transform.rotation,lookRotation, Time.fixedDeltaTime * 0.1f);
        }

        private void Move()
        {
            rb.velocity = transform.up * speed;
        }

        private void SetDirection()
        {
            if (target != null)
            {
                var temp = transform.position - target.transform.position;
                direction = temp;
            }
            else
            {
                direction = Vector2.down;
            }
        }
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            var target = other.gameObject.GetComponent<IDamagable>();
            target?.TakeHit(damage);
            if (other.CompareTag("ObjectDestroyer") || target != null)
            {
                AudioSource.PlayClipAtPoint(explosion, transform.position,volume);
                var explode = Instantiate(boom, transform.position, transform.rotation);
                Destroy(explode.gameObject,0.5f);
                Destroy(this.gameObject);
            }
        }
    }
}
