﻿using Singleton;
using Spaceship;
using TMPro;
using UnityEngine;

namespace Manager
{
    public class ScoreManager : MonoSingleton<ScoreManager>
    {
        [SerializeField] private TextMeshProUGUI scoreText;
        [SerializeField] private TextMeshProUGUI finalScoreText;

        internal int PlayerScore = 0;
        
        public void Init()
        {
            GameManager.Instance.OnRestarted += OnRestarted;
            HideScore(false);
            ResetScore();
            SetScore();
        }

        public void SetScore()
        {
            scoreText.text = $"Score : {PlayerScore}";
        }

        internal void SetFinalScore()
        {
            finalScoreText.text = $"Final Score : {PlayerScore}";
        }

        private void ResetScore()
        {
            PlayerScore = 0;
        }

        internal void AddScore(int score)
        {
            PlayerScore += score;
        }
        
        private void Awake()
        {
            Debug.Assert(scoreText != null, "scoreText cannot null");
        }
        
        private void OnRestarted()
        {
            //finalScoreText.text = $"High Score : {playerScore}";
            GameManager.Instance.OnRestarted -= OnRestarted;
            HideScore(true);
            ResetScore();
            SetScore();
        }

        private void HideScore(bool hide)
        {
            scoreText.gameObject.SetActive(!hide);
        }
    }
}


