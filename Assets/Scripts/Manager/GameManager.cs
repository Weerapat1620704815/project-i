﻿using System;
using System.Collections;
using Singleton;
using Spaceship;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Manager
{
    public class GameManager : MonoSingleton<GameManager>
    {
        [SerializeField] private Button startButton;
        [SerializeField] private Button nextButton;
        [SerializeField] private Button BossButton;
        [SerializeField] private Button endButton;
        [SerializeField] private RectTransform dialog;
        [SerializeField] private RectTransform nextLevel;
        [SerializeField] private RectTransform BossDialog;
        [SerializeField] private RectTransform endDialog;
        [SerializeField] private PlayerSpaceship playerSpaceship;
        [SerializeField] private EnemySpaceship enemySpaceship;
        [SerializeField] private EnemySpaceship bossSpaceship;
        public event Action OnRestarted;
        [SerializeField] private int playerSpaceshipHp;
        [SerializeField] private int playerSpaceshipMoveSpeed;
        [SerializeField] private int enemySpaceshipHp;
        [SerializeField] private int enemySpaceshipMoveSpeed;

        [SerializeField] private int countEnemy;
        [SerializeField] private int bossScore;
        
        private int counting = 0;

        internal PlayerSpaceship spaceShip;
        private EnemySpaceship enemySpaceShip;
        private void Awake()
        {
            Debug.Assert(startButton != null, "startButton cannot be null");
            Debug.Assert(nextButton != null, "nextButton cannot be null");
            Debug.Assert(dialog != null, "dialog cannot be null");
            Debug.Assert(nextLevel != null, "nextLevel cannot be null");
            Debug.Assert(playerSpaceship != null, "playerSpaceship cannot be null");
            Debug.Assert(enemySpaceship != null, "enemySpaceship cannot be null");
            Debug.Assert(playerSpaceshipHp > 0, "playerSpaceship hp has to be more than zero");
            Debug.Assert(playerSpaceshipMoveSpeed > 0, "playerSpaceshipMoveSpeed has to be more than zero");
            Debug.Assert(enemySpaceshipHp > 0, "enemySpaceshipHp has to be more than zero");
            Debug.Assert(enemySpaceshipMoveSpeed > 0, "enemySpaceshipMoveSpeed has to be more than zero");

            startButton.onClick.AddListener(OnStartButtonClicked);
            nextButton.onClick.AddListener(OnNextButtonClicked);
            BossButton.onClick.AddListener(OnBossClicked);
            endButton.onClick.AddListener(OnEndClicked);
        }

        private void OnStartButtonClicked()
        {
            dialog.gameObject.SetActive(false);
            StartGame();
        }

        private void OnNextButtonClicked()
        {
            nextLevel.gameObject.SetActive(false);
            SpawnPlayerSpaceship();
            SpawnEnemySpaceship();
        }

        private void OnBossClicked()
        {
            StartCoroutine(LoadBossScene());
        }

        private void OnEndClicked()
        {
            Quiting();
        }

        private void Quiting()
        {
            if (UnityEditor.EditorApplication.isPlaying)
            {
                UnityEditor.EditorApplication.isPlaying = false;
            }
            else
            {
                Application.Quit();
            }
        }

        private void StartGame()
        {
            ScoreManager.Instance.Init();
            SpawnPlayerSpaceship();
            SpawnEnemySpaceship();
        }
        
        private void SpawnPlayerSpaceship()
        {
            spaceShip = Instantiate(playerSpaceship);
            spaceShip.Init(playerSpaceshipHp, playerSpaceshipMoveSpeed);
            spaceShip.OnExploded += OnPlayerSpaceshipExploded;
        }

        private void OnPlayerSpaceshipExploded()
        {
            DestroyRemainingShips();
            if (SceneManager.GetActiveScene().name == "Game")
            {
                Restart();
            }
            else
            {
                endDialog.gameObject.SetActive(true);
                ScoreManager.Instance.SetFinalScore();
            }
        }

        private void SpawnEnemySpaceship()
        {
            var player = spaceShip.GetComponent<Transform>();
            enemySpaceShip = Instantiate(enemySpaceship);
            enemySpaceShip.Init(enemySpaceshipHp, enemySpaceshipMoveSpeed,player);
            enemySpaceShip.OnExploded += OnEnemySpaceshipExploded;
        }

        private void SpawnBossShip()
        {
            var player = spaceShip.GetComponent<Transform>();
            enemySpaceShip = Instantiate(bossSpaceship);
            enemySpaceShip.Init(1000, enemySpaceshipMoveSpeed,player);
            enemySpaceShip.OnExploded += OnBossDeath;
        }

        private void OnBossDeath()
        {
            DestroyRemainingShips();
            ScoreManager.Instance.AddScore(100);
            endDialog.gameObject.SetActive(true);
            ScoreManager.Instance.SetFinalScore();
        }
        private void OnEnemySpaceshipExploded()
        {
            ScoreManager.Instance.AddScore(1);
            ScoreManager.Instance.SetScore();
            if (ScoreManager.Instance.PlayerScore >= bossScore)
            {
                BossDialog.gameObject.SetActive(true);
            }
            else
            {
                if (counting < countEnemy)
                {
                    counting++;
                    SpawnEnemySpaceship();
                }
                if (counting >= countEnemy)
                {
                    counting = 0;
                    NextLevel();
                }
            }
        }

        private IEnumerator LoadBossScene()
        {
            BossDialog.gameObject.SetActive(false);
            yield return SceneManager.LoadSceneAsync("BossScene");
            SpawnPlayerSpaceship();
            yield return new WaitForSeconds(1);
            SpawnBossShip();
        }

        private void Restart()
        {
            DestroyRemainingShips();
            dialog.gameObject.SetActive(true);
            OnRestarted?.Invoke();
        }

        private void NextLevel()
        {
            DestroyRemainingShips();
            nextLevel.gameObject.SetActive(true);
        }

        private void DestroyRemainingShips()
        {
            var remainingEnemies = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (var enemy in remainingEnemies)
            {
                Destroy(enemy);
            }

            var remainingPlayer = GameObject.FindGameObjectsWithTag("Player");
            foreach (var player in remainingPlayer)
            {
                Destroy(player);
            }
        }

    }
}
