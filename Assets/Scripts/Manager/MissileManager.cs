using Singleton;
using UnityEngine;
using Image = UnityEngine.UI.Image;

namespace Manager
{
    public class MissileManager : MonoSingleton<MissileManager>
    {
        [SerializeField] private Image missile;

        private void Awake()
        {
            missile.enabled = false;
        }

        private void Update()
        {
            if (GameManager.Instance.spaceShip != null)
            {
                missile.enabled = GameManager.Instance.spaceShip.HasMissile;
            }
        }
    
    }
}
