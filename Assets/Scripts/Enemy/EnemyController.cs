﻿using Spaceship;
using UnityEngine;

namespace Enemy
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private EnemySpaceship enemySpaceship;
        [SerializeField] private float chasingThresholdDistance;

        [SerializeField] internal Transform player;
        [SerializeField] private Vector3 delta;

        private void Update()
        {
            MoveToPlayer();
            enemySpaceship.Fire();
        }

         private void MoveToPlayer()
         {
             var temp = transform.position;
             delta =  player.position - temp;
             temp.x += delta.x * (enemySpaceship.Speed * Time.deltaTime);
             transform.position = temp;
         }
    }    
}

