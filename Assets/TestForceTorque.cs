﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestForceTorque : MonoBehaviour
{

    public float force;
    public float torque;

    Rigidbody rb;



    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            rb.AddForce(0,force,0);
        }

        if (Input.GetMouseButtonDown(1))
        {
            rb.AddTorque(0, 0, torque);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            rb.isKinematic = true;
        }
    }
}
